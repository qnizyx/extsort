package org.qnixyz.extsort.util;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.extsort.ExtSorter;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CsvExtSorterTest {

  private static final File _DIR = new File("target/test-classes");

  private static int MAX_CHUNK_SIZE = 100;

  private static int MAX_MERGE_GROUP = 2;

  private static final File T1000_ASC_FILE_EXPECT = StringExtSorterTest.T1000_ASC_FILE_EXPECT;

  private static final File T1000_ASC_FILE_IN = StringExtSorterTest.T1000_ASC_FILE_IN;

  private static final File T1000_ASC_FILE_OUT =
      new File(_DIR, "CsvExtSorterTest.1000.out.ascending");

  private static final File T1000_DESC_FILE_EXPECT = StringExtSorterTest.T1000_DESC_FILE_EXPECT;

  private static final File T1000_DESC_FILE_IN = StringExtSorterTest.T1000_DESC_FILE_IN;

  private static final File T1000_DESC_FILE_OUT =
      new File(_DIR, "CsvExtSorterTest.1000.out.descending");

  private static final File T1000_F2_ASC_FILE_EXPECT =
      new File(_DIR, "CsvExtSorterTest.1000.f2.expect.ascending");

  private static final File T1000_F2_ASC_FILE_IN = StringExtSorterTest.T1000_ASC_FILE_IN;

  private static final File T1000_F2_ASC_FILE_OUT =
      new File(_DIR, "CsvExtSorterTest.1000.f2.out.ascending");

  @Test
  public void readme_3() throws Exception {
    final ExtSorter<Csv> sorter = new ExtSorter<>(new CsvExtSortFactory(Pattern.compile("[-]")),
        new CsvComparator(new CsvFieldComparator(2), new CsvFieldComparator(3)));
    sorter.sort(T1000_F2_ASC_FILE_IN, T1000_F2_ASC_FILE_OUT);
  }

  @Test
  public void test_1000_ascending() throws Exception {
    final ExtSorter<Csv> sorter = new ExtSorter<>(
        new CsvExtSortFactory(Pattern.compile("[-]"), MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR),
        new CsvComparator());
    sorter.sort(T1000_ASC_FILE_IN, T1000_ASC_FILE_OUT);
    assertTrue(
        "The files differ: '" + T1000_ASC_FILE_EXPECT.getPath() + "' and '"
            + T1000_ASC_FILE_OUT.getPath() + "'",
        FileUtils.contentEquals(T1000_ASC_FILE_EXPECT, T1000_ASC_FILE_OUT));
  }

  @Test
  public void test_1000_descending() throws Exception {
    final ExtSorter<Csv> sorter = new ExtSorter<>(
        new CsvExtSortFactory(Pattern.compile("[-]"), MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR),
        new CsvComparator(false));
    sorter.sort(T1000_DESC_FILE_IN, T1000_DESC_FILE_OUT);
    assertTrue(
        "The files differ: '" + T1000_DESC_FILE_EXPECT.getPath() + "' and '"
            + T1000_DESC_FILE_OUT.getPath() + "'",
        FileUtils.contentEquals(T1000_DESC_FILE_EXPECT, T1000_DESC_FILE_OUT));
  }

  @Test
  public void test_1000_f2_ascending() throws Exception {
    final ExtSorter<Csv> sorter = new ExtSorter<>(
        new CsvExtSortFactory(Pattern.compile("[-]"), MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR),
        new CsvComparator(new CsvFieldComparator(2), new CsvFieldComparator(3)));
    sorter.sort(T1000_F2_ASC_FILE_IN, T1000_F2_ASC_FILE_OUT);
    assertTrue(
        "The files differ: '" + T1000_F2_ASC_FILE_EXPECT.getPath() + "' and '"
            + T1000_F2_ASC_FILE_OUT.getPath() + "'",
        FileUtils.contentEquals(T1000_F2_ASC_FILE_EXPECT, T1000_F2_ASC_FILE_OUT));
  }
}
