package org.qnixyz.extsort.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StringComparatorTest {

  @Test
  public void test_default() throws Exception {
    final StringComparator util = new StringComparator();
    assertEquals("2 x empty", 0, util.compare("", ""));
    assertEquals("null and empty", -1, util.compare(null, ""));
    assertEquals("empty and null", 1, util.compare("", null));
  }

  @Test
  public void test_numeric_decimal() throws Exception {
    final StringComparator util = new StringComparator(StringComparePolicy.NUMERIC_DECIMAL);

    assertTrue("x and y", util.compare("x", "y") < 0);
    assertTrue("-x and y", util.compare("-x", "y") < 0);
    assertTrue("x and -y", util.compare("x", "-y") > 0);

    assertEquals("2 x 0", 0, util.compare("0", "0"));
    assertEquals("2 x 0xxx", 0, util.compare("0xxx", "0xxx"));
    assertEquals("2 x - 0", 0, util.compare("- 0", "- 0"));
    assertEquals("2 x - 0xxx", 0, util.compare("- 0xxx", "- 0xxx"));

    assertEquals("2 x 0.", 0, util.compare("0.", "0."));
    assertEquals("2 x 0.xxx", 0, util.compare("0.xxx", "0.xxx"));
    assertEquals("2 x - 0.", 0, util.compare("- 0.", "- 0."));
    assertEquals("2 x - 0.xxx", 0, util.compare("- 0.xxx", "- 0.xxx"));

    assertEquals("2 x .0", 0, util.compare(".0", ".0"));
    assertEquals("2 x .0xxx", 0, util.compare(".0xxx", ".0xxx"));
    assertEquals("2 x - .0", 0, util.compare("- .0", "- .0"));
    assertEquals("2 x - .0xxx", 0, util.compare("- .0xxx", "- .0xxx"));

    assertTrue("0 and 1", util.compare("0", "1") < 0);
    assertTrue("0xxx and 1xxx", util.compare("0xxx", "1xxx") < 0);

    assertTrue("1 and 0", util.compare("1", "0") > 0);
    assertTrue("1xxx and 0xxx", util.compare("1xxx", "0xxx") > 0);

    assertTrue("0 and - 1", util.compare("0", "- 1") > 0);
    assertTrue("0xxx and - 1xxx", util.compare("0xxx", "- 1xxx") > 0);

    assertTrue("0/ and - 001", util.compare("0/", "- 001") > 0);
    assertTrue("0/xxx and - 001xxx", util.compare("0/xxx", "- 001xxx") > 0);

    assertTrue("- 1 and 0", util.compare("- 1", "0") < 0);
    assertTrue("- 1xxx and 0xxx", util.compare("- 1xxx", "0xxx") < 0);
  }

  @Test
  public void test_numeric_hex() throws Exception {
    final StringComparator util = new StringComparator(StringComparePolicy.NUMERIC_HEX);

    assertTrue("x and y", util.compare("x", "y") < 0);
    assertTrue("-x and y", util.compare("-x", "y") < 0);
    assertTrue("x and -y", util.compare("x", "-y") > 0);

    assertEquals("2 x 0", 0, util.compare("0", "0"));
    assertEquals("2 x 0xxx", 0, util.compare("0xxx", "0xxx"));
    assertEquals("2 x - 0", 0, util.compare("- 0", "- 0"));
    assertEquals("2 x - 0xxx", 0, util.compare("- 0xxx", "- 0xxx"));

    assertTrue("0 and 1", util.compare("0", "1") < 0);
    assertTrue("0xxx and 1xxx", util.compare("0xxx", "1xxx") < 0);

    assertTrue("1 and 0", util.compare("1", "0") > 0);
    assertTrue("1xxx and 0xxx", util.compare("1xxx", "0xxx") > 0);

    assertTrue("0 and - 1", util.compare("0", "- 1") > 0);
    assertTrue("0xxx and - 1xxx", util.compare("0xxx", "- 1xxx") > 0);

    assertTrue("a/ and - 00b", util.compare("a/", "- 00b") > 0);
    assertTrue("a/xxx and - 00bxxx", util.compare("a/xxx", "- 00bxxx") > 0);

    assertTrue("- 1 and 0", util.compare("- 1", "0") < 0);
    assertTrue("- 1xxx and 0xxx", util.compare("- 1xxx", "0xxx") < 0);
  }
}
