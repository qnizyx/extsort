package org.qnixyz.extsort.util;

import static org.junit.Assert.assertTrue;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.extsort.ExtSorter;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StringExtSorterTest {

  private static final File _DIR = new File("target/test-classes");

  private static int MAX_CHUNK_SIZE = 100;

  private static int MAX_MERGE_GROUP = 2;

  protected static final File T1000_ASC_FILE_EXPECT =
      new File(_DIR, "StringExtSorterTest.1000.expect.ascending");

  protected static final File T1000_ASC_FILE_IN = new File(_DIR, "StringExtSorterTest.1000.in");

  private static final File T1000_ASC_FILE_OUT =
      new File(_DIR, "StringExtSorterTest.1000.out.ascending");

  protected static final File T1000_DESC_FILE_EXPECT =
      new File(_DIR, "StringExtSorterTest.1000.expect.descending");

  protected static final File T1000_DESC_FILE_IN = new File(_DIR, "StringExtSorterTest.1000.in");

  private static final File T1000_DESC_FILE_OUT =
      new File(_DIR, "StringExtSorterTest.1000.out.descending");

  @Test
  public void test_1000_ascending() throws Exception {
    final ExtSorter<String> sorter = new ExtSorter<>(
        new StringExtSortFactory(MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR), new StringComparator());
    sorter.sort(T1000_ASC_FILE_IN, T1000_ASC_FILE_OUT);
    assertTrue(
        "The files differ: '" + T1000_ASC_FILE_EXPECT.getPath() + "' and '"
            + T1000_ASC_FILE_OUT.getPath() + "'",
        FileUtils.contentEquals(T1000_ASC_FILE_EXPECT, T1000_ASC_FILE_OUT));
  }

  @Test
  public void test_1000_descending() throws Exception {
    final ExtSorter<String> sorter =
        new ExtSorter<>(new StringExtSortFactory(MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR),
            new StringComparator(false));
    sorter.sort(T1000_DESC_FILE_IN, T1000_DESC_FILE_OUT);
    assertTrue(
        "The files differ: '" + T1000_DESC_FILE_EXPECT.getPath() + "' and '"
            + T1000_DESC_FILE_OUT.getPath() + "'",
        FileUtils.contentEquals(T1000_DESC_FILE_EXPECT, T1000_DESC_FILE_OUT));
  }

  @Test
  public void test_readme_1() throws Exception {
    final ExtSorter<String> sorter =
        new ExtSorter<>(new StringExtSortFactory(), new StringComparator());
    sorter.sort(T1000_ASC_FILE_IN, T1000_ASC_FILE_OUT);
  }

  @Test
  public void test_readme_2() throws Exception {
    final ExtSorter<String> sorter =
        new ExtSorter<>(new StringExtSortFactory(), new StringComparator(true));
    sorter.sort(T1000_DESC_FILE_IN, T1000_DESC_FILE_OUT);
  }
}
