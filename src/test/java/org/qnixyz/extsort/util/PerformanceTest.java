package org.qnixyz.extsort.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeFalse;
import static org.junit.Assume.assumeTrue;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.UUID;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.extsort.ExtSorter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PerformanceTest {

  private static class Stats {

    private static Stats read(final File file) throws Exception {
      try (BufferedReader w = new BufferedReader(
          new InputStreamReader(new BufferedInputStream(new FileInputStream(file)),
              StringExtSortFactory.DEFAULT_CHARSET))) {
        for (final String line = w.readLine(); line != null;) {
          return new Gson().fromJson(line, Stats.class);
        }
        throw new IllegalStateException("Cannot read: " + file);
      }
    }

    private long unixSortDurationInMs;

    private Stats() {}

    private Stats(final long unixSortDurationInMs) {
      this.unixSortDurationInMs = unixSortDurationInMs;
    }

    private void write(final File file) throws Exception {
      try (BufferedWriter w = new BufferedWriter(
          new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(file)),
              StringExtSortFactory.DEFAULT_CHARSET))) {
        final Gson gson = new GsonBuilder().create();
        w.write(gson.toJson(this).toString());
        w.write(LineSeparator.UNIX.getLineSeparator());
      }
    }
  }

  private static final File _DIR = new File("target/test-classes");

  public static final String DO_TEST_PROP = "DO_EXTSORT_PERFORMANCE_TEST";

  private static final int LINES = 10000000;

  private static int MAX_CHUNK_SIZE = 100000;

  private static int MAX_MERGE_GROUP = 16;

  private static final File TEST_CSV_EXP = new File(_DIR, "PerformanceTest.csv.expect");

  private static final File TEST_CSV_OUT = new File(_DIR, "PerformanceTest.csv.out");

  private static final File TEST_CSV_STATS = new File(_DIR, "PerformanceTest.csv.stats");

  private static final File TEST_IN = new File(_DIR, "PerformanceTest.in");

  private static final File TEST_STRING_EXP = new File(_DIR, "PerformanceTest.string.expect");

  private static final File TEST_STRING_OUT = new File(_DIR, "PerformanceTest.string.out");

  private static final File TEST_STRING_STATS = new File(_DIR, "PerformanceTest.string.stats");

  private static final File TMP = new File(_DIR, "PerformanceTest.tmp");

  @BeforeClass
  public static void beforeMethod() {
    assumeFalse(System.getProperty("os.name").toLowerCase().contains("win"));
    assumeTrue(System.getenv(DO_TEST_PROP) != null
        && System.getenv(DO_TEST_PROP).equalsIgnoreCase("true"));
  }

  private void createCsvExpectFile() throws Exception {
    if (TEST_CSV_EXP.exists()) {
      return;
    }
    final String cmd = "sort -t - -k 2 " + TEST_IN + " -o " + TMP;
    final long start = System.currentTimeMillis();
    final Runtime runtime = Runtime.getRuntime();
    final Process process = runtime.exec(cmd);
    for (int b = process.getInputStream().read(); b != -1; b = process.getInputStream().read()) {
      System.out.write(b);
    }
    for (int b = process.getErrorStream().read(); b != -1; b = process.getErrorStream().read()) {
      System.err.write(b);
    }
    final int exit = process.waitFor();
    final long stop = System.currentTimeMillis();
    assertEquals("Command did not exit with 0: " + cmd, 0, exit);
    if (!TMP.renameTo(TEST_CSV_EXP)) {
      throw new IOException("Failed to rename file '" + TMP + "' into '" + TEST_CSV_EXP + "'");
    }
    final long duration = stop - start;
    final Stats stats = new Stats(duration);
    stats.write(TEST_CSV_STATS);
  }

  private void createInputTestFile() throws Exception {
    if (TEST_IN.exists()) {
      return;
    }
    try (BufferedWriter w = new BufferedWriter(
        new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(TMP)),
            StringExtSortFactory.DEFAULT_CHARSET))) {
      for (int i = 0; i < LINES; i++) {
        w.write(UUID.randomUUID().toString());
        w.write(LineSeparator.UNIX.getLineSeparator());
      }
    }
    if (!TMP.renameTo(TEST_IN)) {
      throw new IOException("Failed to rename file '" + TMP + "' into '" + TEST_IN + "'");
    }
  }

  private void createStringExpectFile() throws Exception {
    if (TEST_STRING_EXP.exists()) {
      return;
    }
    final String cmd = "sort " + TEST_IN + " -o " + TMP;
    final long start = System.currentTimeMillis();
    final Runtime runtime = Runtime.getRuntime();
    final Process process = runtime.exec(cmd);
    for (int b = process.getInputStream().read(); b != -1; b = process.getInputStream().read()) {
      System.out.write(b);
    }
    for (int b = process.getErrorStream().read(); b != -1; b = process.getErrorStream().read()) {
      System.err.write(b);
    }
    final int exit = process.waitFor();
    final long stop = System.currentTimeMillis();
    assertEquals("Command did not exit with 0: " + cmd, 0, exit);
    if (!TMP.renameTo(TEST_STRING_EXP)) {
      throw new IOException("Failed to rename file '" + TMP + "' into '" + TEST_STRING_EXP + "'");
    }
    final long duration = stop - start;
    final Stats stats = new Stats(duration);
    stats.write(TEST_STRING_STATS);
  }

  @Test
  public void test_csv() throws Exception {
    createInputTestFile();
    createCsvExpectFile();
    final Stats stats = Stats.read(TEST_CSV_STATS);
    final long start = System.currentTimeMillis();
    final ExtSorter<Csv> sorter = new ExtSorter<>(
        new CsvExtSortFactory(Pattern.compile("[-]"), MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR),
        new CsvComparator(new CsvFieldComparator(2), new CsvFieldComparator(3),
            new CsvFieldComparator(4), new CsvFieldComparator(5)));
    sorter.sort(TEST_IN, TEST_CSV_OUT);
    final long stop = System.currentTimeMillis();
    final long duration = stop - start;
    assertTrue(
        "The files differ: '" + TEST_CSV_EXP.getPath() + "' and '" + TEST_CSV_OUT.getPath() + "'",
        FileUtils.contentEquals(TEST_CSV_EXP, TEST_CSV_OUT));
    System.out.println("Duration CSV UNIX sort: " + stats.unixSortDurationInMs + "ms.");
    System.out.println("Duration CSV ExtSorter: " + duration + "ms.");
    System.out
        .println("Duration CSV ExtSorter/UNIX sort: " + duration / stats.unixSortDurationInMs);
  }

  @Test
  public void test_line() throws Exception {
    createInputTestFile();
    createStringExpectFile();
    final Stats stats = Stats.read(TEST_STRING_STATS);
    final long start = System.currentTimeMillis();
    final ExtSorter<String> sorter = new ExtSorter<>(
        new StringExtSortFactory(MAX_CHUNK_SIZE, MAX_MERGE_GROUP, _DIR), new StringComparator());
    sorter.sort(TEST_IN, TEST_STRING_OUT);
    final long stop = System.currentTimeMillis();
    final long duration = stop - start;
    assertTrue("The files differ: '" + TEST_STRING_EXP.getPath() + "' and '"
        + TEST_STRING_OUT.getPath() + "'",
        FileUtils.contentEquals(TEST_STRING_EXP, TEST_STRING_OUT));
    System.out.println("Duration STRING UNIX sort: " + stats.unixSortDurationInMs + "ms.");
    System.out.println("Duration STRING ExtSorter: " + duration + "ms.");
    System.out
        .println("Duration STRING ExtSorter/UNIX sort: " + duration / stats.unixSortDurationInMs);
  }
}
