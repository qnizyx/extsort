package org.qnixyz.extsort;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ForkJoinPool;

/**
 * Abstract class {@link ExtSortFactory} must be extended for the type of record at hand.
 *
 * @author Vincenzo Zocca
 *
 * @param <T> The type of record contained in the file to sort
 */
public abstract class ExtSortFactory<T> {

  private final ForkJoinPool forkJoinPool;

  private final int maxChunkSize;

  private final int maxMergeGroupSize;

  private final boolean parallel;

  private final File tmp;

  /**
   * Constructor accepting the max. size attributes.
   *
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   */
  public ExtSortFactory(final int maxChunkSize, final int maxMergeGroupSize) {
    this(maxChunkSize, maxMergeGroupSize, null);
  }

  /**
   * Constructor accepting the max. size attributes and the temporary directory to use.
   *
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. If {@code null} then the system default temporary
   *        directory is used through system property {@code "java.io.tmpdir"}. Optional.
   */
  public ExtSortFactory(final int maxChunkSize, final int maxMergeGroupSize, final File tmp) {
    this(maxChunkSize, maxMergeGroupSize, tmp, true, null);
  }

  /**
   * Constructor accepting the max. size attributes, the temporary directory to use, a parallel
   * processing flag and a fork join pool.
   *
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. If {@code null} then the system default temporary
   *        directory is used through system property {@code "java.io.tmpdir"}. Optional.
   * @param parallel flag to process in parallel
   * @param forkJoinPool fork join pool. Optional. If {@code null} and {@code parallel==true} then
   *        the common fork join pool is used.
   */
  public ExtSortFactory(final int maxChunkSize, final int maxMergeGroupSize, final File tmp,
      final boolean parallel, final ForkJoinPool forkJoinPool) {
    if (maxChunkSize < 1) {
      throw new IllegalArgumentException(
          "Illegal value for parameter maxChunkSize. It must be greater than 0 but is "
              + maxChunkSize);
    }
    if (maxMergeGroupSize < 2) {
      throw new IllegalArgumentException(
          "Illegal value for parameter maxMergeGroup. It must be greater than 1 but is "
              + maxMergeGroupSize);
    }
    this.maxChunkSize = maxChunkSize;
    this.maxMergeGroupSize = maxMergeGroupSize;
    this.tmp = checkTmp(tmp == null ? new File(System.getProperty("java.io.tmpdir")) : tmp);
    this.parallel = parallel;
    this.forkJoinPool = forkJoinPool;
  }

  private File checkTmp(final File tmp) {
    if (!tmp.exists()) {
      throw new IllegalArgumentException("File in parameter tmp does not exist: '" + tmp + "'");
    }
    if (!tmp.isDirectory()) {
      throw new IllegalArgumentException("File in parameter tmp is not a directory: '" + tmp + "'");
    }
    if (!tmp.canRead()) {
      throw new IllegalArgumentException(
          "Directory in parameter tmp cannot be read: '" + tmp + "'");
    }
    if (!tmp.canWrite()) {
      throw new IllegalArgumentException(
          "Directory in parameter tmp cannot be written: '" + tmp + "'");
    }
    return tmp;
  }

  /**
   * Factory method to create {@link ExtSortReader} objects.
   *
   * @param is input stream to read from
   * @return an instance of {@link ExtSortReader}
   * @throws IOException should one occur
   */
  public abstract ExtSortReader<T> createReader(InputStream is) throws IOException;

  /**
   * Factory method to create {@link ExtSortWriter} objects.
   *
   * @param os output stream to write to
   * @return an instance of {@link ExtSortWriter}
   * @throws IOException should one occur
   */
  public abstract ExtSortWriter<T> createWriter(OutputStream os) throws IOException;

  public ForkJoinPool getForkJoinPool() {
    return forkJoinPool;
  }

  /**
   * Returns the maximum chunk size in numbers of records.
   *
   * @return maximum chunk size in numbers of records
   */
  public int getMaxChunkSize() {
    return maxChunkSize;
  }

  /**
   * Returns the maximum number of chunks than can be merged at once.
   *
   * @return maximum number of chunks than can be merged at once
   */
  public int getMaxMergeGroupSize() {
    return maxMergeGroupSize;
  }

  /**
   * Returns the temporary directory to use
   *
   * @return temporary directory to use
   */
  public File getTmp() {
    return tmp;
  }

  public boolean isParallel() {
    return parallel;
  }
}
