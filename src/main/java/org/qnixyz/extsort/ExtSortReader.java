package org.qnixyz.extsort;

import java.io.Closeable;
import java.io.IOException;

/**
 * Interface {@linkplain ExtSortReader} is used to read objects from input streams.
 *
 * @author Vincenzo Zocca
 *
 * @param <T> The type of record contained in the file to sort
 */
public interface ExtSortReader<T> extends Closeable {

  /**
   * Returns the next available record.
   *
   * @return next available record
   * @throws IOException should one occur
   */
  public T read() throws IOException;
}
