package org.qnixyz.extsort;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.qnixyz.extsort.util.CsvExtSortFactory;
import org.qnixyz.extsort.util.StringExtSortFactory;

/**
 * Class {@link ExtSorter} implements external sorting through a k-way merge algorithm.
 * <p>
 * First the input file/stream is partitioned in chunks which are written in disk an which contain a
 * set of sorted records. The set of all chinks from a file is called a heap. Indeed all records of
 * any initial chunk should fit in memory and this property can be set using {@link ExtSortFactory}.
 * <p>
 * Then, as long as the heap contains more chunks than can be merged at once, the heap is reduced
 * through merging chunks. The number of chunks that can be merged also is a property that can be
 * set using {@link ExtSortFactory}.
 * <p>
 * Finally the remaining heap is merged into the output file/stream.
 * <p>
 * See reference implementations {@link CsvExtSortFactory} and {@link StringExtSortFactory}
 *
 * @author Vincenzo Zocca
 *
 * @param <T> The type of record contained in the file to sort
 */
public class ExtSorter<T> {

  private class Chunk {

    private final File file;

    private Chunk(final File file) {
      this.file = file;
    }

    private Chunk(final File file, final Chunk other) throws IOException {
      this.file = file;
      mv(other.file, file);
    }
  }

  private class ChunkMergeGroup {

    private final Ctx ctx;

    private final List<Chunk> list = new ArrayList<>();

    private ChunkMergeGroup(final Ctx ctx) {
      this.ctx = ctx;
    }

    private void add(final Chunk chunk) {
      list.add(chunk);
    }

    private void closeReaders(final List<ExtSortReader<T>> readers) throws IOException {
      for (final ExtSortReader<T> reader : readers) {
        reader.close();
      }
    }

    private boolean containsNonNull(final T[] values) {
      for (final T value : values) {
        if (value != null) {
          return true;
        }
      }
      return false;
    }

    private T getFirstValue(final T[] values) {
      T ret = null;
      int retI = -1;
      for (int i = 0; i < values.length; i++) {
        final T next = values[i];
        if (ret == null) {
          ret = next;
          retI = i;
          continue;
        }
        if (next == null) {
          continue;
        }
        if (cmp.compare(ret, next) > 0) {
          ret = next;
          retI = i;
        }
      }
      values[retI] = null;
      return ret;
    }

    private boolean isEmpty() {
      return list.isEmpty();
    }

    private boolean isFull() {
      return list.size() >= maxMergeGroupSize;
    }

    private Chunk merge() throws IOException {
      if (list.size() == 1) {
        return new Chunk(ctx.createTmpFile("chunk"), list.get(0));
      }
      final File file = ctx.createTmpFile("chunk");
      try (ExtSortWriter<T> w = fact.createWriter(openFileOutputStream(file, true))) {
        write(w);
      }
      return new Chunk(file);
    }

    private List<ExtSortReader<T>> openReaders() throws IOException {
      final List<ExtSortReader<T>> ret = new ArrayList<>();
      for (final Chunk chunk : list) {
        ret.add(fact.createReader(openFileInputStream(chunk.file, true)));
      }
      return ret;
    }

    @SuppressWarnings("unchecked")
    private T[] readValues(final List<ExtSortReader<T>> readers) throws IOException {
      final Object[] ret = new Object[readers.size()];
      for (int i = 0; i < readers.size(); i++) {
        ret[i] = readers.get(i).read();
      }
      return (T[]) ret;
    }

    private void updateValues(final List<ExtSortReader<T>> readers, final T[] values)
        throws IOException {
      for (int i = 0; i < readers.size(); i++) {
        if (values[i] == null) {
          values[i] = readers.get(i).read();
        }
      }
    }

    private void write(final ExtSortWriter<T> out) throws IOException {
      final List<ExtSortReader<T>> readers = openReaders();
      try {
        final T[] values = readValues(readers);
        while (containsNonNull(values)) {
          final T value = getFirstValue(values);
          out.write(value);
          updateValues(readers, values);
        }
      } finally {
        closeReaders(readers);
      }
    }
  }

  private class ChunkWriter {

    private final File file;

    private final List<T> list = new ArrayList<>();

    private ChunkWriter(final Ctx ctx) throws IOException {
      this.file = ctx.createTmpFile("chunk");
    }

    private void add(final T e) {
      list.add(e);
    }

    private boolean isEmpty() {
      return list.isEmpty();
    }

    private boolean isFull() {
      return list.size() >= maxChunkSize;
    }

    private void sort() {
      list.sort(cmp);
    }

    private void write() throws IOException {
      sort();
      try (ExtSortWriter<T> w = fact.createWriter(openFileOutputStream(file, true))) {
        for (final T e : list) {
          w.write(e);
        }
      }
    }
  }

  private class Ctx {

    private final File dir;

    private final File file;

    private int n = 1;

    private final String runId;

    private final FilePathsWriter usedFiles;

    private Ctx() throws IOException {
      this.runId = UUID.randomUUID().toString();
      this.dir = new File(tmp, String.format("ExtSorter.tmp.%s", runId));
      this.file = createTmpFile("ctx", false);
      mkdir(dir);
      usedFiles = new FilePathsWriter(file);
    }

    private void clear() throws IOException {
      usedFiles.close();
      try (FilePathsReader r = new FilePathsReader(usedFiles.file)) {
        if (parallel) {
          forkJoinPool.submit(() -> {
            r.stream().parallel().forEach(file -> {
              try {
                rm(file);
              } catch (final IOException e) {
                throw new UncheckedIOException(e);
              }
            });
          }).join();
        } else {
          r.stream().forEach(file -> {
            try {
              rm(file);
            } catch (final IOException e) {
              throw new UncheckedIOException(e);
            }
          });
        }
      }
      rm(file);
      rmdir(dir);
    }

    private File createTmpFile(final String type) throws IOException {
      return createTmpFile(type, true);
    }

    private synchronized File createTmpFile(final String type, final boolean addToUsedFiles)
        throws IOException {
      final File ret = new File(dir, String.format("%06d.%s", n++, type));
      if (addToUsedFiles) {
        usedFiles.write(ret);
      }
      return ret;
    }
  }

  private class FilePathsReader implements Closeable {

    private BufferedReader r;

    private FilePathsReader(final File file) throws IOException {
      r = new BufferedReader(new InputStreamReader(openFileInputStream(file, true)));
    }

    @Override
    public void close() throws IOException {
      if (r != null) {
        r.close();
        r = null;
      }
    }

    private File read() throws IOException {
      if (r == null) {
        return null;
      }
      final String path = r.readLine();
      if (path == null) {
        return null;
      }
      return new File(path);
    }

    private Stream<File> stream() {
      final Iterator<File> iter = new Iterator<File>() {
        File next = null;

        @Override
        public boolean hasNext() {
          if (next != null) {
            return true;
          } else {
            try {
              next = read();
              return next != null;
            } catch (final IOException e) {
              throw new UncheckedIOException(e);
            }
          }
        }

        @Override
        public File next() {
          if (next != null || hasNext()) {
            final File ret = next;
            next = null;
            return ret;
          } else {
            throw new NoSuchElementException();
          }
        }
      };
      return StreamSupport.stream(
          Spliterators.spliteratorUnknownSize(iter, Spliterator.ORDERED | Spliterator.NONNULL),
          false);
    }
  }

  private class FilePathsWriter implements Closeable {

    private final File file;

    private BufferedWriter h;

    private FilePathsWriter(final File file) throws IOException {
      this.file = file;
      h = new BufferedWriter(new OutputStreamWriter(openFileOutputStream(file, true)));
    }

    @Override
    public void close() throws IOException {
      if (h != null) {
        h.close();
        h = null;
      }
    }

    private synchronized void write(final File file) throws IOException {
      h.write(file.getPath());
      h.newLine();
    }
  }

  private class Heap implements Closeable {

    private final Ctx ctx;

    private final File file;

    private FilePathsReader r;

    private final int size;

    private Heap(final Ctx ctx, final HeapWriter hw) throws IOException {
      this.ctx = ctx;
      this.file = hw.file;
      this.r = new FilePathsReader(file);
      this.size = hw.size;
    }

    @Override
    public void close() throws IOException {
      if (r != null) {
        r.close();
        r = null;
      }
    }

    private void deleteFiles() throws IOException {
      if (r != null) {
        throw new IllegalStateException("This is a bug. FilePathsReader isn't closed.");
      }
      try (FilePathsReader r = new FilePathsReader(file)) {
        if (parallel) {
          forkJoinPool.submit(() -> {
            r.stream().parallel().forEach(file -> {
              try {
                rm(file);
              } catch (final IOException e) {
                throw new UncheckedIOException(e);
              }
            });
          }).join();
        } else {
          r.stream().forEach(file -> {
            try {
              rm(file);
            } catch (final IOException e) {
              throw new UncheckedIOException(e);
            }
          });
        }
      }
    }

    private Heap merge() throws IOException {
      final HeapWriter hw = new HeapWriter(ctx);
      final Stream<ExtSorter<T>.ChunkMergeGroup> chunkMergeGroups = streamChunkMergeGroups();
      if (parallel) {
        forkJoinPool.submit(() -> {
          chunkMergeGroups.parallel().forEach(e -> mergeElement(e, hw));
        }).join();
      } else {
        chunkMergeGroups.forEach(e -> mergeElement(e, hw));
      }
      hw.close();
      return new Heap(ctx, hw);
    }

    private void mergeElement(final ExtSorter<T>.ChunkMergeGroup e,
        final ExtSorter<T>.HeapWriter hw) {
      try {
        hw.add(e.merge());
      } catch (final IOException exc) {
        throw new UncheckedIOException(exc);
      }
    }

    private ChunkMergeGroup readChunkMergeGroup() throws IOException {
      if (r == null) {
        return null;
      }
      final ChunkMergeGroup ret = new ChunkMergeGroup(ctx);
      for (File file = r.read(); file != null; file = r.read()) {
        ret.add(new Chunk(file));
        if (ret.isFull()) {
          return ret;
        }
      }
      if (ret.isEmpty()) {
        return null;
      }
      return ret;
    }

    private Stream<ChunkMergeGroup> streamChunkMergeGroups() {
      final Iterator<ChunkMergeGroup> iter = new Iterator<ChunkMergeGroup>() {
        ChunkMergeGroup next = null;

        @Override
        public boolean hasNext() {
          if (next != null) {
            return true;
          } else {
            try {
              next = readChunkMergeGroup();
              return next != null;
            } catch (final IOException e) {
              throw new UncheckedIOException(e);
            }
          }
        }

        @Override
        public ChunkMergeGroup next() {
          if (next != null || hasNext()) {
            final ChunkMergeGroup ret = next;
            next = null;
            return ret;
          } else {
            throw new NoSuchElementException();
          }
        }
      };
      return StreamSupport.stream(
          Spliterators.spliteratorUnknownSize(iter, Spliterator.ORDERED | Spliterator.NONNULL),
          false);
    }
  }

  private class HeapWriter implements Closeable {

    private final File file;

    private int size;

    private FilePathsWriter w;

    private HeapWriter(final Ctx ctx) throws IOException {
      this.file = ctx.createTmpFile("heap");
      w = new FilePathsWriter(file);
    }

    private void add(final Chunk c) throws IOException {
      w.write(c.file);
      size++;
    }

    private void add(final ChunkWriter cw) throws IOException {
      w.write(cw.file);
      size++;
    }

    @Override
    public void close() throws IOException {
      if (w != null) {
        w.close();
        w = null;
      }
    }
  }

  private class NoCloseInputStream extends InputStream {

    private final InputStream is;

    private NoCloseInputStream(final InputStream is) {
      this.is = is;
    }

    @Override
    public int available() throws IOException {
      return is.available();
    }

    @Override
    public void close() throws IOException {}

    @Override
    public synchronized void mark(final int readlimit) {
      is.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
      return is.markSupported();
    }

    @Override
    public int read() throws IOException {
      return is.read();
    }

    @Override
    public int read(final byte[] b) throws IOException {
      return is.read(b);
    }

    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
      return is.read(b, off, len);
    }

    @Override
    public synchronized void reset() throws IOException {
      is.reset();
    }

    @Override
    public long skip(final long n) throws IOException {
      return is.skip(n);
    }
  }

  private class NoCloseOutputStream extends OutputStream {

    private final OutputStream os;

    private NoCloseOutputStream(final OutputStream is) {
      this.os = is;
    }

    @Override
    public void close() throws IOException {}

    @Override
    public void flush() throws IOException {
      os.flush();
    }

    @Override
    public void write(final byte[] b) throws IOException {
      os.write(b);
    }

    @Override
    public void write(final byte[] b, final int off, final int len) throws IOException {
      os.write(b, off, len);
    }

    @Override
    public void write(final int b) throws IOException {
      os.write(b);
    }
  }

  private final Comparator<T> cmp;

  private final ExtSortFactory<T> fact;

  private final ForkJoinPool forkJoinPool;

  private final int maxChunkSize;

  private final int maxMergeGroupSize;

  private final boolean parallel;

  private final File tmp;

  /**
   * The constructor accepts a factory and a comparator.
   *
   * @param fact instance of {@link ExtSortFactory}. Mandatory.
   * @param cmp instance of {@link Comparator}. Mandatory.
   */
  public ExtSorter(final ExtSortFactory<T> fact, final Comparator<T> cmp) {
    this.fact = Objects.requireNonNull(fact);
    this.cmp = Objects.requireNonNull(cmp);
    this.maxChunkSize = fact.getMaxChunkSize();
    this.maxMergeGroupSize = fact.getMaxMergeGroupSize();
    this.tmp = fact.getTmp();
    this.parallel = fact.isParallel();
    if (parallel) {
      this.forkJoinPool = makeForkJoinPool();
    } else {
      this.forkJoinPool = null;
    }
  }

  private Heap createHeap(final Ctx ctx, final ExtSortReader<T> r) throws IOException {
    try (HeapWriter hw = new HeapWriter(ctx)) {
      ChunkWriter cw = new ChunkWriter(ctx);
      for (T e = r.read(); e != null; e = r.read()) {
        if (cw.isFull()) {
          cw.write();
          hw.add(cw);
          cw = new ChunkWriter(ctx);
          cw.add(e);
          continue;
        }
        cw.add(e);
      }
      if (!cw.isEmpty()) {
        cw.write();
        hw.add(cw);
      }
      hw.close();
      return new Heap(ctx, hw);
    }
  }

  private ForkJoinPool makeForkJoinPool() {
    if (fact.getForkJoinPool() == null) {
      return ForkJoinPool.commonPool();
    }
    return fact.getForkJoinPool();
  }

  private void mkdir(final File dir) {
    if (!dir.mkdir()) {
      throw new IllegalStateException("Failed to create directory '" + dir + "'");
    }
  }

  private void mv(final File src, final File dst) throws IOException {
    if (!src.renameTo(dst)) {
      throw new IOException("Failed to rename '" + src + "' into '" + dst + "'");
    }
  }

  private InputStream openFileInputStream(final File file, final boolean gzip) throws IOException {
    InputStream ret = new BufferedInputStream(new FileInputStream(file));
    if (gzip) {
      ret = new GZIPInputStream(ret);
    }
    return ret;
  }

  private OutputStream openFileOutputStream(final File file, final boolean gzip)
      throws IOException {
    OutputStream ret = new BufferedOutputStream(new FileOutputStream(file));
    if (gzip) {
      ret = new GZIPOutputStream(ret);
    }
    return ret;
  }

  private void rm(final File file) throws IOException {
    if (file.exists() && !file.delete()) {
      throw new IOException("Failed to delete file '" + file + "'");
    }
  }

  private void rmdir(final File dir) throws IOException {
    if (dir.exists()) {
      for (final File file : dir.listFiles()) {
        if (file.isFile()) {
          rm(file);
        } else {
          rmdir(file);
        }
      }
      rm(dir);
    }
  }

  /**
   * Method to perform external sort.
   *
   * @param in the input file. Mandatory.
   * @param inGzip the flag stating the input file is gzipped
   * @param out the output file. Mandatory.
   * @param outGzip the flag stating the output file must be gzipped
   * @throws IOException should one occur
   */
  public void sort(final File in, final boolean inGzip, final File out, final boolean outGzip)
      throws IOException {
    Objects.requireNonNull(in);
    Objects.requireNonNull(out);
    try ( //
        InputStream is = openFileInputStream(in, inGzip); //
        OutputStream os = openFileOutputStream(out, outGzip); //
    ) {
      sort(is, os);
    }
  }

  /**
   * Method to perform external sort.
   *
   * @param in the input file. Mandatory.
   * @param out the output file. Mandatory.
   * @throws IOException should one occur
   */
  public void sort(final File in, final File out) throws IOException {
    Objects.requireNonNull(in);
    Objects.requireNonNull(out);
    sort(in, false, out, false);
  }

  /**
   * Method to perform external sort.
   *
   * @param in the input stream. Mandatory.
   * @param out the output stream. Mandatory.
   * @throws IOException should one occur
   */
  public void sort(final InputStream in, final OutputStream out) throws IOException {
    Objects.requireNonNull(in);
    Objects.requireNonNull(out);
    final Ctx ctx = new Ctx();
    try ( //
        ExtSortReader<T> r = fact.createReader(new NoCloseInputStream(in)); //
        ExtSortWriter<T> w = fact.createWriter(new NoCloseOutputStream(out)); //
    ) {
      Heap heap = createHeap(ctx, r);
      while (heap.size > maxMergeGroupSize) {
        final Heap next = heap.merge();
        heap.close();
        heap.deleteFiles();
        heap = next;
      }
      if (heap.size > 0) {
        final ExtSorter<T>.ChunkMergeGroup cmg = heap.readChunkMergeGroup();
        cmg.write(w);
      }
      heap.close();
    } finally {
      ctx.clear();
    }
  }
}
