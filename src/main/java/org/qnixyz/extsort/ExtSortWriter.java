package org.qnixyz.extsort;

import java.io.Closeable;
import java.io.IOException;

/**
 * Interface {@linkplain ExtSortWriter} is used to write objects onto output streams.
 *
 * @author Vincenzo Zocca
 *
 * @param <T> The type of record contained in the file to sort
 */
public interface ExtSortWriter<T> extends Closeable {

  /**
   * Writes a record.
   *
   * @param o record to write
   * @throws IOException should one occur
   */
  public void write(T o) throws IOException;
}
