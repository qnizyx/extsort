package org.qnixyz.extsort.util;

public enum StringComparePolicy {
  /**
   * Policy to sort lexicographically
   */
  LEXICOGRAPHICALLY,

  /**
   * Policy to sort lexicographically, ignoring case
   */
  LEXICOGRAPHICALLY_IGNORE_CASE,

  /**
   * Policy to sort decimal numerically
   */
  NUMERIC_DECIMAL,

  /**
   * Policy to sort hexadecimal numerically
   */
  NUMERIC_HEX;
}
