package org.qnixyz.extsort.util;

/**
 * Enumeration {@link LineSeparator} is
 *
 * @author Vincenzo Zocca
 */
public enum LineSeparator {

  /**
   * Mac OS line separator ({@code "\r"})
   */
  MAC("\r"),

  /**
   * JRE syastem line separator
   */
  SYSTEM(null),

  /**
   * UNIX line separator ({@code "\n"})
   */
  UNIX("\n"),

  /**
   * Windows line separator ({@code "\r\n"})
   */
  WINDOWS("\r\n");

  private String lineSeparator;

  private LineSeparator(final String lineSeparator) {
    if (lineSeparator == null) {
      this.lineSeparator = System.lineSeparator();
    } else {
      this.lineSeparator = lineSeparator;
    }
  }

  /**
   * Returns the line separator string.
   *
   * @return line separator string
   */
  public String getLineSeparator() {
    return lineSeparator;
  }
}
