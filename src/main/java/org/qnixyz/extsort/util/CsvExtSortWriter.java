package org.qnixyz.extsort.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Objects;
import org.qnixyz.extsort.ExtSortWriter;

/**
 * Class {@link CsvExtSortWriter} implements interface {@link ExtSortWriter} for object type
 * {@link Csv}.
 *
 * @author Vincenzo Zocca
 */
public class CsvExtSortWriter implements ExtSortWriter<Csv> {

  private final LineSeparator lineSeparator;

  private BufferedWriter w;

  /**
   * Constructor accepting the character, the line separator and the output stream.
   *
   * @param charset character set. Mandatory.
   * @param lineSeparator line separator. Mandatory.
   * @param os output stream. Mandatory.
   */
  public CsvExtSortWriter(final Charset charset, final LineSeparator lineSeparator,
      final OutputStream os) {
    Objects.requireNonNull(charset);
    Objects.requireNonNull(os);
    this.lineSeparator = Objects.requireNonNull(lineSeparator);
    w = new BufferedWriter(new OutputStreamWriter(os, charset));
  }

  @Override
  public void close() throws IOException {
    if (w != null) {
      w.close();
      w = null;
    }
  }

  @Override
  public void write(final Csv o) throws IOException {
    w.write(o.getLine());
    w.write(lineSeparator.getLineSeparator());
  }
}
