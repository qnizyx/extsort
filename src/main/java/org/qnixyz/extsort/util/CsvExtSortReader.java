package org.qnixyz.extsort.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.regex.Pattern;
import org.qnixyz.extsort.ExtSortReader;

/**
 * Class {@link CsvExtSortReader} implements interface {@link ExtSortReader} for object type
 * {@link Csv}.
 *
 * @author Vincenzo Zocca
 */
public class CsvExtSortReader implements ExtSortReader<Csv> {

  private final Pattern cleanHead;

  private final Pattern cleanTail;

  private BufferedReader r;

  private final Pattern separator;

  /**
   * Constructor accepting a field separator pattern, a pattern to clean the head of the line, a
   * pattern to clean the tail of the line, the character set and the input stream.
   *
   * @param separator field separator pattern. Mandatory.
   * @param cleanHead pattern to clean the head of the line. Optional.
   * @param cleanTail pattern to clean the tail of the line. Optional.
   * @param charset character set. Mandatory.
   * @param is input stream. Mandatory.
   */
  public CsvExtSortReader(final Pattern separator, final Pattern cleanHead, final Pattern cleanTail,
      final Charset charset, final InputStream is) {
    this.separator = Objects.requireNonNull(separator);
    this.cleanHead = cleanHead;
    this.cleanTail = cleanTail;
    Objects.requireNonNull(charset);
    Objects.requireNonNull(is);
    r = new BufferedReader(new InputStreamReader(is, charset));
  }

  @Override
  public void close() throws IOException {
    if (r != null) {
      r.close();
      r = null;
    }
  }

  @Override
  public Csv read() throws IOException {
    if (r == null) {
      return null;
    }
    final String line = r.readLine();
    if (line == null) {
      return null;
    }
    return new Csv(line, separator, cleanHead, cleanTail);
  }
}
