package org.qnixyz.extsort.util;

import static org.qnixyz.extsort.util.StringExtSortFactory.DEFAULT_CHARSET;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.ForkJoinPool;
import java.util.regex.Pattern;
import org.qnixyz.extsort.ExtSortFactory;
import org.qnixyz.extsort.ExtSortReader;
import org.qnixyz.extsort.ExtSortWriter;

/**
 * Class {@link CsvExtSortFactory} extends abstract class {@link ExtSortFactory} for object type
 * {@link Csv}.
 *
 * @author Vincenzo Zocca
 */
public class CsvExtSortFactory extends ExtSortFactory<Csv> {

  /**
   * Default maximum chunk size
   */
  public static final int DEFAULT_MAX_CHUNK_SIZE = 10000;

  /**
   * Default maximum number of chunks than can be merged at once
   */
  public static final int DEFAULT_MAX_MERGE_GROUP_SIZE = 16;

  private final Charset charset;

  private final Pattern cleanHead;

  private final Pattern cleanTail;

  private final Pattern fieldSeparator;

  private final LineSeparator lineSeparator;

  /**
   * Constructor accepting a line separator and a field separator pattern.
   *
   * @param lineSeparator line separator pattern. Mandatory.
   * @param fieldSeparator field separator pattern. Mandatory.
   */
  public CsvExtSortFactory(final LineSeparator lineSeparator, final Pattern fieldSeparator) {
    this(lineSeparator, fieldSeparator, DEFAULT_CHARSET, DEFAULT_MAX_CHUNK_SIZE,
        DEFAULT_MAX_MERGE_GROUP_SIZE);
  }

  /**
   * Constructor accepting a line separator, a field separator pattern, the character set, the
   * maximum chunk size and the maximum number of chunks than can be merged at once.
   *
   * @param lineSeparator line separator pattern. Mandatory.
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param charset character set. Mandatory.
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   */
  public CsvExtSortFactory(final LineSeparator lineSeparator, final Pattern fieldSeparator,
      final Charset charset, final int maxChunkSize, final int maxMergeGroupSize) {
    this(lineSeparator, fieldSeparator, null, null, charset, maxChunkSize, maxMergeGroupSize, null);
  }

  /**
   * Constructor accepting a line separator, a field separator pattern, the character set, the
   * maximum chunk size, the maximum number of chunks than can be merged at once and the temporary
   * directory to use.
   *
   * @param lineSeparator line separator pattern. Mandatory.
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public CsvExtSortFactory(final LineSeparator lineSeparator, final Pattern fieldSeparator,
      final int maxChunkSize, final int maxMergeGroupSize, final File tmp) {
    this(lineSeparator, fieldSeparator, null, null, DEFAULT_CHARSET, maxChunkSize,
        maxMergeGroupSize, tmp);
  }

  /**
   * Constructor accepting a line separator, a field separator pattern, a pattern to clean the head
   * of the line, a pattern to clean the tail of a line, the character set, the maximum chunk size,
   * the maximum number of chunks than can be merged at once and the temporary directory to use.
   *
   * @param lineSeparator line separator pattern. Mandatory.
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param cleanHead pattern to clean the head of the line. Optional.
   * @param cleanTail pattern to clean the tail of the line. Optional.
   * @param charset character set. Mandatory.
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public CsvExtSortFactory(final LineSeparator lineSeparator, final Pattern fieldSeparator,
      final Pattern cleanHead, final Pattern cleanTail, final Charset charset,
      final int maxChunkSize, final int maxMergeGroupSize, final File tmp) {
    super(maxChunkSize, maxMergeGroupSize, tmp);
    this.lineSeparator = Objects.requireNonNull(lineSeparator);
    this.fieldSeparator = Objects.requireNonNull(fieldSeparator);
    this.cleanHead = cleanHead;
    this.cleanTail = cleanTail;
    this.charset = Objects.requireNonNull(charset);
  }

  /**
   * Constructor accepting a line separator, a field separator pattern, a pattern to clean the head
   * of the line, a pattern to clean the tail of a line, the character set, the maximum chunk size,
   * the maximum number of chunks than can be merged at once, the temporary directory to use, a
   * parallel processing flag and a fork join pool.
   *
   * @param lineSeparator line separator pattern. Mandatory.
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param cleanHead pattern to clean the head of the line. Optional.
   * @param cleanTail pattern to clean the tail of the line. Optional.
   * @param charset character set. Mandatory.
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   * @param parallel flag to process in parallel
   * @param forkJoinPool fork join pool. Optional. If {@code null} and {@code parallel==true} then
   *        the common fork join pool is used.
   */
  public CsvExtSortFactory(final LineSeparator lineSeparator, final Pattern fieldSeparator,
      final Pattern cleanHead, final Pattern cleanTail, final Charset charset,
      final int maxChunkSize, final int maxMergeGroupSize, final File tmp, final boolean parallel,
      final ForkJoinPool forkJoinPool) {
    super(maxChunkSize, maxMergeGroupSize, tmp, parallel, forkJoinPool);
    this.lineSeparator = Objects.requireNonNull(lineSeparator);
    this.fieldSeparator = Objects.requireNonNull(fieldSeparator);
    this.cleanHead = cleanHead;
    this.cleanTail = cleanTail;
    this.charset = Objects.requireNonNull(charset);
  }

  /**
   * Constructor accepting a field separator pattern.
   *
   * @param fieldSeparator field separator pattern. Mandatory.
   */
  public CsvExtSortFactory(final Pattern fieldSeparator) {
    this(LineSeparator.SYSTEM, fieldSeparator, DEFAULT_CHARSET, DEFAULT_MAX_CHUNK_SIZE,
        DEFAULT_MAX_MERGE_GROUP_SIZE);
  }

  /**
   * Constructor a field separator pattern and the character set.
   *
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param charset character set. Mandatory.
   */
  public CsvExtSortFactory(final Pattern fieldSeparator, final Charset charset) {
    this(LineSeparator.SYSTEM, fieldSeparator, charset, DEFAULT_MAX_CHUNK_SIZE,
        DEFAULT_MAX_MERGE_GROUP_SIZE);
  }

  /**
   * Constructor accepting a line separator, the character set, the maximum chunk size, the maximum
   * number of chunks than can be merged at once and the temporary directory to use.
   *
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param charset character set. Mandatory.
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public CsvExtSortFactory(final Pattern fieldSeparator, final Charset charset,
      final int maxChunkSize, final int maxMergeGroupSize, final File tmp) {
    this(LineSeparator.SYSTEM, fieldSeparator, null, null, charset, maxChunkSize, maxMergeGroupSize,
        tmp);
  }

  /**
   * Constructor accepting a field separator pattern, the maximum chunk size, the maximum number of
   * chunks than can be merged at once and the temporary directory to use.
   *
   * @param fieldSeparator field separator pattern. Mandatory.
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public CsvExtSortFactory(final Pattern fieldSeparator, final int maxChunkSize,
      final int maxMergeGroupSize, final File tmp) {
    this(LineSeparator.SYSTEM, fieldSeparator, null, null, DEFAULT_CHARSET, maxChunkSize,
        maxMergeGroupSize, tmp);
  }

  @Override
  public ExtSortReader<Csv> createReader(final InputStream is) {
    return new CsvExtSortReader(fieldSeparator, cleanHead, cleanTail, charset, is);
  }

  @Override
  public ExtSortWriter<Csv> createWriter(final OutputStream os) {
    return new CsvExtSortWriter(charset, lineSeparator, os);
  }
}
