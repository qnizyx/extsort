package org.qnixyz.extsort.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Objects;
import org.qnixyz.extsort.ExtSortWriter;

/**
 * Class {@link StringExtSortWriter} implements interface {@link ExtSortWriter} for object type
 * {@link String}.
 *
 * @author Vincenzo Zocca
 */
public class StringExtSortWriter implements ExtSortWriter<String> {

  private final LineSeparator lineSeparator;

  private BufferedWriter w;

  /**
   * /** Constructor accepting the character, the line separator and the output stream.
   *
   * @param charset character set. Mandatory.
   * @param lineSeparator line separator. Mandatory.
   * @param os output stream. Mandatory.
   */
  public StringExtSortWriter(final Charset charset, final LineSeparator lineSeparator,
      final OutputStream os) {
    Objects.requireNonNull(charset);
    Objects.requireNonNull(os);
    this.lineSeparator = Objects.requireNonNull(lineSeparator);
    w = new BufferedWriter(new OutputStreamWriter(os, charset));
  }

  @Override
  public void close() throws IOException {
    if (w != null) {
      w.close();
      w = null;
    }
  }

  @Override
  public void write(final String o) throws IOException {
    w.write(o);
    w.write(lineSeparator.getLineSeparator());
  }
}
