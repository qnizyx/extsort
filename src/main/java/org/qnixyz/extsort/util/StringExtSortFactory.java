package org.qnixyz.extsort.util;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.ForkJoinPool;
import org.qnixyz.extsort.ExtSortFactory;
import org.qnixyz.extsort.ExtSortReader;
import org.qnixyz.extsort.ExtSortWriter;

/**
 * Class {@link StringExtSortFactory} extends abstract class {@link ExtSortFactory} for object type
 * {@link String}.
 *
 * @author Vincenzo Zocca
 */
public class StringExtSortFactory extends ExtSortFactory<String> {

  /**
   * The default character set.
   */
  public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

  /**
   * Default maximum chunk size
   */
  public static final int DEFAULT_MAX_CHUNK_SIZE = 10000;

  /**
   * Default maximum number of chunks than can be merged at once
   */
  public static final int DEFAULT_MAX_MERGE_GROUP_SIZE = 16;

  private final Charset charset;

  private final LineSeparator lineSeparator;

  /**
   * Empty constructor.
   */
  public StringExtSortFactory() {
    this(LineSeparator.SYSTEM, DEFAULT_CHARSET, DEFAULT_MAX_CHUNK_SIZE,
        DEFAULT_MAX_MERGE_GROUP_SIZE, null);
  }

  /**
   * Constructor accepting the character set.
   *
   * @param charset character set
   */
  public StringExtSortFactory(final Charset charset) {
    this(LineSeparator.SYSTEM, charset, DEFAULT_MAX_CHUNK_SIZE, DEFAULT_MAX_MERGE_GROUP_SIZE, null);
  }

  /**
   * Constructor accepting the character set, the maximum chunk size in numbers of records, the
   * maximum number of chunks than can be merged at once and the temporary directory to use.
   *
   * @param charset character set
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public StringExtSortFactory(final Charset charset, final int maxChunkSize,
      final int maxMergeGroupSize, final File tmp) {
    this(LineSeparator.SYSTEM, charset, maxChunkSize, maxMergeGroupSize, tmp);
  }

  /**
   * Constructor the maximum chunk size, the maximum number of chunks than can be merged at once and
   * the temporary directory to use.
   *
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public StringExtSortFactory(final int maxChunkSize, final int maxMergeGroupSize, final File tmp) {
    this(LineSeparator.SYSTEM, DEFAULT_CHARSET, maxChunkSize, maxMergeGroupSize, tmp);
  }

  /**
   * Constructor accepting a line separator.
   *
   * @param lineSeparator line separator
   */
  public StringExtSortFactory(final LineSeparator lineSeparator) {
    this(lineSeparator, DEFAULT_CHARSET, DEFAULT_MAX_CHUNK_SIZE, DEFAULT_MAX_MERGE_GROUP_SIZE,
        null);
  }

  /**
   * Constructor accepting a line separator, the character set, the maximum chunk size, the maximum
   * number of chunks than can be merged at once and the temporary directory to use.
   *
   * @param lineSeparator line separator
   * @param charset character set
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public StringExtSortFactory(final LineSeparator lineSeparator, final Charset charset,
      final int maxChunkSize, final int maxMergeGroupSize, final File tmp) {
    this(lineSeparator, charset, maxChunkSize, maxMergeGroupSize, tmp, true, null);
  }

  /**
   * Constructor accepting a line separator, the character set, the maximum chunk size, the maximum
   * number of chunks than can be merged at once, the temporary directory to use, a parallel
   * processing flag and a fork join pool.
   *
   * @param lineSeparator line separator
   * @param charset character set
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   * @param parallel flag to process in parallel
   * @param forkJoinPool fork join pool. Optional. If {@code null} and {@code parallel==true} then
   *        the common fork join pool is used.
   */
  public StringExtSortFactory(final LineSeparator lineSeparator, final Charset charset,
      final int maxChunkSize, final int maxMergeGroupSize, final File tmp, final boolean parallel,
      final ForkJoinPool forkJoinPool) {
    super(maxChunkSize, maxMergeGroupSize, tmp, parallel, forkJoinPool);
    this.charset = Objects.requireNonNull(charset);
    this.lineSeparator = Objects.requireNonNull(lineSeparator);
  }

  /**
   * Constructor accepting a line separator, the maximum chunk size, the maximum number of chunks
   * than can be merged at once and the temporary directory to use.
   *
   * @param lineSeparator line separator
   * @param maxChunkSize maximum chunk size in numbers of records
   * @param maxMergeGroupSize maximum number of chunks than can be merged at once
   * @param tmp temporary directory to use. Optional.
   */
  public StringExtSortFactory(final LineSeparator lineSeparator, final int maxChunkSize,
      final int maxMergeGroupSize, final File tmp) {
    this(lineSeparator, DEFAULT_CHARSET, maxChunkSize, maxMergeGroupSize, tmp);
  }

  @Override
  public ExtSortReader<String> createReader(final InputStream is) {
    return new StringExtSortReader(charset, is);
  }

  @Override
  public ExtSortWriter<String> createWriter(final OutputStream os) {
    return new StringExtSortWriter(charset, lineSeparator, os);
  }
}
