package org.qnixyz.extsort.util;

import java.util.Comparator;
import java.util.Objects;

/**
 * Class {@link CsvComparator} compares {@link Csv} objects.
 *
 * @author Vincenzo Zocca
 */
public class CsvComparator implements Comparator<Csv> {

  private final int ascending;

  private final StringComparator defaultStringComparator;

  private final CsvFieldComparator[] fieldComparators;

  /**
   * Constructor accepting ascending sort flag and field comparators.
   *
   * @param ascending ascending sort flag
   * @param fieldComparators field comparators
   */
  public CsvComparator(final boolean ascending, final CsvFieldComparator... fieldComparators) {
    this(ascending, new StringComparator(), fieldComparators);
  }

  /**
   * Constructor accepting ascending sort flag, the default string comparator and field comparators.
   *
   * @param ascending ascending sort flag
   * @param defaultStringComparator default string comparator. Used when all field comparators do
   *        not yield a difference.
   * @param fieldComparators field comparators
   */
  public CsvComparator(final boolean ascending, final StringComparator defaultStringComparator,
      final CsvFieldComparator... fieldComparators) {
    this.ascending = ascending ? 1 : -1;
    this.defaultStringComparator = Objects.requireNonNull(defaultStringComparator);
    this.fieldComparators = fieldComparators;
  }

  /**
   * Constructor accepting field comparators.
   *
   * @param fieldComparators field comparators
   */
  public CsvComparator(final CsvFieldComparator... fieldComparators) {
    this(true, fieldComparators);
  }

  @Override
  public int compare(final Csv o1, final Csv o2) {
    int ret = 0;
    if (o1 == o2) {
      ret = 0;
    } else if (o1 == null) {
      ret = -1;
    } else if (o2 == null) {
      ret = 1;
    }
    if (ret != 0) {
      return ret * ascending;
    }
    if (fieldComparators != null) {
      for (final CsvFieldComparator fieldComparator : fieldComparators) {
        ret = compare(fieldComparator, o1, o2);
        if (ret != 0) {
          break;
        }
      }
    }
    if (ret != 0) {
      return ret * ascending;
    }
    return defaultStringComparator.compare(o1.getLine(), o2.getLine()) * ascending;
  }

  private int compare(final CsvFieldComparator fieldComparator, final Csv o1, final Csv o2) {
    if (fieldComparator == null) {
      return 0;
    }
    final String f1 = o1.getField(fieldComparator.getFieldNumber());
    final String f2 = o2.getField(fieldComparator.getFieldNumber());
    return fieldComparator.getStringComparator().compare(f1, f2);
  }
}
