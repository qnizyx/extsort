package org.qnixyz.extsort.util;

import java.util.Objects;

/**
 * Class {@link CsvFieldComparator} implements a comparator for CSV fields.
 *
 * @author Vincenzo Zocca
 */
public class CsvFieldComparator {

  private final boolean ascending;

  private final int fieldNumber;

  private final StringComparator stringComparator;

  /**
   * Constructor accepting the field number.
   *
   * @param fieldNumber field number
   */
  public CsvFieldComparator(final int fieldNumber) {
    this(fieldNumber, new StringComparator(), true);
  }

  /**
   * Constructor accepting the field number, a string comparator and the ascending sort flag.
   *
   * @param fieldNumber field number
   * @param stringComparator string comparator. Mandatory.
   * @param ascending ascending sort flag
   */
  public CsvFieldComparator(final int fieldNumber, final StringComparator stringComparator,
      final boolean ascending) {
    if (fieldNumber < 1) {
      throw new IllegalArgumentException(
          "Parameter 'fieldNumber' is not a positive integer: " + fieldNumber);
    }
    this.fieldNumber = fieldNumber;
    this.stringComparator = Objects.requireNonNull(stringComparator);
    this.ascending = ascending;
  }

  /**
   * Returns the field number.
   *
   * @return field number
   */
  public int getFieldNumber() {
    return fieldNumber;
  }

  /**
   * Returns the string comparator.
   *
   * @return string comparator
   */
  public StringComparator getStringComparator() {
    return stringComparator;
  }

  /**
   * Returns the ascending sort flag.
   *
   * @return ascending sort flag
   */
  public boolean isAscending() {
    return ascending;
  }
}
