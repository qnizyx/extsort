package org.qnixyz.extsort.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Objects;
import org.qnixyz.extsort.ExtSortReader;

/**
 * Class {@link StringExtSortReader} implements interface {@link ExtSortReader} for object type
 * {@link String}.
 *
 * @author Vincenzo Zocca
 */
public class StringExtSortReader implements ExtSortReader<String> {

  private BufferedReader r;

  /**
   * Constructor accepting the character set and the input stream.
   *
   * @param charset character set
   * @param is input stream
   */
  public StringExtSortReader(final Charset charset, final InputStream is) {
    Objects.requireNonNull(charset);
    Objects.requireNonNull(is);
    r = new BufferedReader(new InputStreamReader(is, charset));
  }

  @Override
  public void close() throws IOException {
    if (r != null) {
      r.close();
      r = null;
    }
  }

  @Override
  public String read() throws IOException {
    if (r == null) {
      return null;
    }
    return r.readLine();
  }
}
