package org.qnixyz.extsort.util;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class {@link Csv} contains information on CSV lines.
 *
 * @author Vincenzo Zocca
 */
public class Csv {

  private final String[] fields;

  private final String line;

  /**
   * Constructor accepting a line and a separator pattern.
   *
   * @param line line string. Mandatory.
   * @param separator line separator pattern. Mandatory.
   */
  public Csv(final String line, final Pattern separator) {
    this(line, separator, null, null);
  }

  /**
   * Constructor accepting a line, a separator pattern, a pattern to clean the head of the line and
   * a pattern to clean the tail of a line.
   *
   * @param line line string. Mandatory.
   * @param separator field separator pattern. Mandatory.
   * @param cleanHead pattern to clean the head of the line. Optional.
   * @param cleanTail pattern to clean the tail of the line. Optional.
   */
  public Csv(String line, final Pattern separator, final Pattern cleanHead,
      final Pattern cleanTail) {
    this.line = Objects.requireNonNull(line);
    Objects.requireNonNull(separator);
    if (cleanHead != null) {
      final Matcher m = cleanHead.matcher(line);
      if (m.matches()) {
        line = m.replaceAll(line);
      }
    }
    if (cleanTail != null) {
      final Matcher m = cleanTail.matcher(line);
      if (m.matches()) {
        line = m.replaceAll(line);
      }
    }
    fields = separator.split(line);
  }

  /**
   * Returns the field value.
   *
   * @param fieldNumber field number
   * @return field value. {@code null} if not found.
   */
  public String getField(final int fieldNumber) {
    if (fields.length >= fieldNumber) {
      return fields[fieldNumber - 1];
    }
    return null;
  }

  public String getLine() {
    return line;
  }
}
