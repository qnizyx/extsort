package org.qnixyz.extsort.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class {@link CsvComparator} compares {@link String} objects.
 *
 * @author Vincenzo Zocca
 */
public class StringComparator implements Comparator<String> {

  private static final Pattern DECIMAL_HEAD = Pattern.compile("^(-?)\\s*(\\d*\\.?\\d*).*$");

  private static final Pattern HEX_HEAD = Pattern.compile("^(-?)\\s*([\\da-f]*).*$");

  private static final Pattern LEADING_ZEROES = Pattern.compile("^0*");

  private static final Pattern NON_DECIMAL_HEAD = Pattern.compile("^[^-\\d.]");

  private static final Pattern NON_HEX_HEAD =
      Pattern.compile("^[^-\\d.a-f]", Pattern.CASE_INSENSITIVE);

  private final int ascending;

  private final StringComparePolicy comparePolicy;

  /**
   * Default constructor.
   */
  public StringComparator() {
    this(StringComparePolicy.LEXICOGRAPHICALLY, true);
  }

  /**
   * Constructor accepting the ascending sort flag.
   *
   * @param ascending ascending sort flag
   */
  public StringComparator(final boolean ascending) {
    this(StringComparePolicy.LEXICOGRAPHICALLY, ascending);
  }

  /**
   * Constructor accepting the string compare policy.
   *
   * @param comparePolicy string compare policy
   */
  public StringComparator(final StringComparePolicy comparePolicy) {
    this(comparePolicy, true);
  }

  /**
   * Constructor accepting the string compare policy and the ascending sort flag.
   *
   * @param comparePolicy string compare policy
   * @param ascending ascending sort flag
   */
  public StringComparator(final StringComparePolicy comparePolicy, final boolean ascending) {
    this.comparePolicy = Objects.requireNonNull(comparePolicy);
    this.ascending = ascending ? 1 : -1;
  }

  @Override
  public int compare(final String o1, final String o2) {
    int ret = 0;
    if (o1 == o2) {
      ret = 0;
    } else if (o1 == null) {
      ret = -1;
    } else if (o2 == null) {
      ret = 1;
    }
    if (ret != 0) {
      return ret * ascending;
    }

    switch (comparePolicy) {
      case LEXICOGRAPHICALLY: {
        ret = o1.compareTo(o2);
        break;
      }
      case LEXICOGRAPHICALLY_IGNORE_CASE: {
        ret = o1.compareToIgnoreCase(o2);
        break;
      }
      case NUMERIC_DECIMAL: {
        ret = compareNumericDecimal(o1, o2);
        break;
      }
      case NUMERIC_HEX: {
        ret = compareNumericHex(o1, o2);
        break;
      }
      default: {
        throw new IllegalStateException(
            "This is a bug. Field 'comparePolicy' is set to unsupported value: " + comparePolicy);
      }
    }

    return ret * ascending;
  }

  private int compareNumericDecimal(final String o1, final String o2) {
    final BigDecimal n1 = toBigDecimalFromNumericDecimal(o1);
    final BigDecimal n2 = toBigDecimalFromNumericDecimal(o2);
    if (n1 == n2) {
      return o1.compareTo(o2);
    } else if (n1 == null) {
      return -1;
    } else if (n2 == null) {
      return 1;
    }
    final int ret = n1.compareTo(n2);
    return ret;
  }

  private int compareNumericHex(final String o1, final String o2) {
    final BigInteger n1 = toBigIntegerFromNumericHex(o1);
    final BigInteger n2 = toBigIntegerFromNumericHex(o2);
    if (n1 == n2) {
      return o1.compareTo(o2);
    } else if (n1 == null) {
      return -1;
    } else if (n2 == null) {
      return 1;
    }
    final int ret = n1.compareTo(n2);
    return ret;
  }

  private BigDecimal toBigDecimalFromNumericDecimal(String str) {

    // Strip non decimal characters from head
    Matcher m = NON_DECIMAL_HEAD.matcher(str);
    str = m.replaceFirst("");

    // Match for decimal number
    m = DECIMAL_HEAD.matcher(str);
    if (!m.matches()) {
      return null;
    }

    // No number
    if (m.group(2).equals("")) {
      return null;
    }

    // Negative
    final boolean negative = m.group(1).equals("-");

    // Clean decimal part
    m = LEADING_ZEROES.matcher(m.group(2));
    String decimal = m.replaceFirst("");
    if (decimal.contentEquals("")) {
      decimal = "0";
    } else if (decimal.startsWith(".")) {
      decimal = "0" + decimal;
    }

    // Add leading minus
    if (negative) {
      decimal = "-" + decimal;
    }

    return new BigDecimal(decimal);
  }

  private BigInteger toBigIntegerFromNumericHex(String str) {

    // Strip non hex characters from head
    Matcher m = NON_HEX_HEAD.matcher(str);
    str = m.replaceFirst("");

    // Match for hex number
    m = HEX_HEAD.matcher(str);
    if (!m.matches()) {
      return null;
    }

    // No number
    if (m.group(2).equals("")) {
      return null;
    }

    // Negative
    final boolean negative = m.group(1).equals("-");

    // Clean decimal part
    m = LEADING_ZEROES.matcher(m.group(2));
    String hex = m.replaceFirst("");
    if (hex.contentEquals("")) {
      hex = "0";
    }

    // Add leading minus
    if (negative) {
      hex = "-" + hex;
    }

    return new BigInteger(hex, 16);
  }
}
