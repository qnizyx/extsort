# ExtSorter

ExtSorter is a Java component to sort large files or input streams.

##### Author/Copyright

Vincenzo Zocca

Qnixyz GmbH

Switzerland

qnixyz@qnixyz.org

## 1 TL;DR

You have a large file and you want to sort the lines lexicographically, very much -but not exactly- like a with a UNIX `sort` command:

```
final ExtSorter<String> sorter = new ExtSorter<>(new StringExtSortFactory(), new StringComparator());
sorter.sort(FILE_IN, FILE_OUT);
 ```
 
You want to reverse the order of the sorting, very much -but not exactly- like a with a UNIX `sort -r` command:

```
final ExtSorter<String> sorter = new ExtSorter<>(new StringExtSortFactory(), new StringComparator(true));
sorter.sort(FILE_IN, FILE_OUT);
```

You have a large CSV file and you want to sort the lines using the fields, very much -but not exactly- like with a UNIX `sort -t <sep> -k <field-nr1>` command:

```
final ExtSorter<Csv> sorter = new ExtSorter<>(new CsvExtSortFactory(Pattern.compile("[-]")), new CsvComparator(new CsvFieldComparator(2), new CsvFieldComparator(3)));
sorter.sort(FILE_IN, FILE_OUT);
```

Also supported:

 - Character sets
 - Line separators
 - Descending order
 - Decimal numerical sort
 - Hexadecimal numerical sort
 - Parallel processing

You can also use `ExtSorter` for other types of file for which you need to implement a set of interfaces.

## 2 Main goal

The main goal of this component is to allow sorting of files without breaking the memory bank. ExtSorter goes to great lengths to avoid using memory. At most a set of objects is loaded into a list to order a chunk. Even the list of chunks is not kept in memory but is stored in files to avoid extremely large files to kill the memory through the administrative information.

Performance seems to be acceptable.

## 3 Concept

### 3.1 Class `ExtSorter`

Class `ExtSorter` implements the actual external sorting. It divides up the input file into chunks and merges these until the output file/stream can be created.

### 3.2 Interface `ExtSortFactory`

Interface `ExtSortFactory` must be implemented to let class `ExtSorter` create reader and writers to read/write objects from files.

### 3.3 Interface `ExtSortReader`

Interface `ExtSortReader` must be implemented and class `ExtSortFactory` creates and returns its instances.

### 3.3 Interface `ExtSortWriter`

Interface `ExtSortWriter` must be implemented and class `ExtSortFactory` creates and returns its instances.

## 4 Algorithm

Class `ExtSorter` implements external sorting through a k-way merge algorithm.

First the input file/stream is partitioned in chunks which are written in disk an which contain a set of sorted records. The set of all chinks from a file is called a heap. Indeed all records of any initial chunk should fit in memory and this property can be set using `ExtSortFactory`.

Then, as long as the heap contains more chunks than can be merged at once, the heap is reduced through merging chunks. The number of chunks that can be merged also is a property that can be set using `ExtSortFactory`.

Finally the remaining heap is merged into the output file/stream.

## 5 Reference Implementations

### 5.1 Line Sorting

Simple line sorting is implemented through classes `StringComparator`, `StringExtSortFactory`, `StringExtSortReader` and `StringExtSortWriter`.

### 5.2 Line Sorting in a CSV file

Line sorting in a CSV file is implemented through classes `Csv`, `CsvComparator`, `CsvExtSortFactory`, `CsvExtSortReader` and `CsvExtSortWriter`.

## 6 On Resource Usage

Three settings mainly influence the resource usage.

### 6.1 Maximum Chunk Size `ExtSortFactory.maxChunkSize`

During the initial state a chunk of data is read into a list for sorting and hence are loaded into memory. Make sure you application can handle the amount of objects into one list. A number that is too small results in a huge amount of chunks being created and their administration overhead should be considered.

Consider sorting lines in a file. The average line length may vary considerably. Therefore the default settings in both `StringExtSortFactory` and `CsvExtSortReader` may not work perfectly. The again, settings may be changed at will.

### 6.2 Maximum Merge Group Size `ExtSortFactory.maxMergeGroupSize`

In the second phase merge groups are built and each group will have this maximum of chunks. Each merge thread will open a maximum of this amount of file handles for reading and one for writing. So parallel processing and the thread pool are also factors in determining the usage of file handles.

### 6.3 Parallelism `ExtSortFactory.parallel` and `ExtSortFactory.forkJoinPool`

Parallelism can be controlled with these two settings and affects the merging phase the most.

## 7 Roll your own

As long as the files you deal with can be read and written as a stream of objects, you should be able to implement sorting for the files using this component.

"Simply" implement for objects of your class:

- A comparator
- Extend and implement `ExtSortFactory`
- Implement a `ExtSortReader`
- Implement a `ExtSortWriter`

The source code of the reference Implementations `CsvExtSortFactory` and `StringExtSortFactory` may be of help.

## 8 Performance

In short: Comparing ExtSorter with UNIX command line is significantly more favorable (for ExtSorter) that when calling UNIX sort from Java.

A simple performance test is defined in `org.qnixyz.extsort.util.PerformanceTest`. First a file with 1'000'000 random UUIDs is generated. Then the file is sorted on the second part (`sort -t - -k 2`) using both UNIX sort and ExtSorter. Another test is done that simply sorts the lines.

When running UNIX sort through `java.lang.Runtime` and `java.lang.Process` results in the following numbers:

- Duration CSV UNIX sort: 8796ms.
- Duration CSV ExtSorter: 78953ms.
- Duration CSV ExtSorter/UNIX sort: 8
- Duration STRING UNIX sort: 7727ms.
- Duration STRING ExtSorter: 55554ms.
- Duration STRING ExtSorter/UNIX sort: 7

The peculiar thing is that running UNIX sort from the command line is much slower.

```
$ time sort -t - -k 2 -o PerformanceTest.csv.out.unix PerformanceTest.in
real	0m54.144s
user	3m1.576s
sys	0m1.788s
```

That amounts to 77776 ms and then:

- STRING ExtSorter/UNIX sort = 78953 / 77776 = 1.02

```
$ time sort -o PerformanceTest.string.out.unix PerformanceTest.in
real	0m53.557s
user	3m6.048s
sys	0m1.562s
```

That amounts to a total user time 82248 ms and then:

- STRING ExtSorter/UNIX sort = 55554 / 82248 = 0.68
